# Template for Adding New Instruction.

This module contains dictionary with Key-value pairs, which makes parsing
easy in the [adding instruction](../add_custom_instr.py) part.

## Quick Help guide
Examples for Name 		 - No Help here :wink: <br>
Examples for format 	 - [here](https://raw.githubusercontent.com/riscv/riscv-opcodes/master/opcodes)<br>
Examples for behaviour - [here](https://github.com/riscv/riscv-isa-sim/tree/master/riscv/insns)<br>
Examples for Type 		 - [here](https://raw.githubusercontent.com/riscv/riscv-isa-sim/master/spike_main/disasm.cc)<br>
Examples for opcode    - [here](https://raw.githubusercontent.com/riscv/riscv-binutils-gdb/riscv-binutils-2.31.1/opcodes/riscv-opc.c)<br>

## Attributes of `Value` in *Key-Value* pair :

#### name:
Name of the Instruction to be added to toolchain
-  Avoid the usage of RISC-V standard instruction set names
- Keep the Instruction names short and simple, since it creeps into the
	entire toolchain.

#### format:
 Format of the Instruction that needs to be added to the toolchain
* This format is added to the `riscv-tools/riscv-opcodes/opcodes` file
* For Adding Instruction to spike - this is mandatory
* For Adding Instruction to assembler - this is mandatory
* For examples consider looking into `/riscv-opcodes/opcodes`file

> `Match` and `Mask` will be generated based on the above format by the [script](../add_custom_instr.py).

#### behaviour:
 Behaviour of the instruction for spike to perform
* `value` is list of strings that should be added to the file
* the behaviour file is created in `/riscv-isa-sim/riscv/insns/<instruction_name>.h`
* references for other instructions can be found in the above directory
* For Adding Instruction to spike - this is mandatory
* Keep the language syntax and semantics correct, else spike will spit errors.

#### type:
Instruction type that needs to be added to the Spike
* This `type` will be added to `riscv-isa-sim/spike_main/disasm.cc`
* DEFINE_$TYPE can be any format as specified in the above directory
* For Adding Instruction to spike - this is mandatory

#### opcode:
Opcode of the instruction that needs to be added to assembler
* This `opcode` will be added to `riscv-gnu-toolchain/riscv-binutils-gdb/opcodes/riscv-opc.c`
* Find references from the above directory for different opcode types
* For Adding Instruction to assembler - this is mandatory

## Example:
``` python
'mac':{
	'name':'mac',
	'format':'mac      rd rs1 rs2 31..25=1 14..12=0 6..2=0x1A 1..0=3',
	'behaviour':[
		"require_extension('M');",
		"reg_t tmp = sext_xlen(RS1 * RS2);",
		"WRITE_RD(sext_xlen(READ_REG(insn.rd()) + tmp));"
		],
	'type':'DEFINE_RTYPE(mac)',
	'opcode':'{"mac",       "I",   "d,s,t",  MATCH_MAC, MASK_MAC, match_opcode, 0 }'
},
```
Note:	Follow python `Dictionary` Syntax intact to avoid build errors.
