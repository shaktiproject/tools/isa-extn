#!/usr/bin/env python3
"""Template for Adding New Instruction.

This module contains only dictionary with Key-value pair, which makes parsing 
easy in the adding instruction part.

Attributes:
    name (str): Name of the Instruction to be added to toolchain
    	* Avoid the usage of RISC-V standard instruction set names
    	* Keep the Instruction names short and simple, since it creeps into the 
    	entire toolchain.
    	``Example`` :  mod

    format(str): Format of the Instruction that needs to be added to the toolchain
    	* This format is added to the `riscv-tools/riscv-opcodes/opcodes` file
    	* For Adding Instruction to spike - this is mandatory
    	* For Adding Instruction to assembler - this is mandatory
    	* For examples consider looking into `/riscv-opcodes/opcodes`file
		Note: 
			Match and Mask will be generated based on the above format by the script
			that's parsing this file.
    behaviour(str): Behaviour of the instruction for spike to perform
    	* `value` is list of strings that should be added to the file
    	* the behaviour file is created in `/riscv-isa-sim/riscv/insns/<instruction_name>.h`
    	* references for other instructions can be found in the above directory
    	* For Adding Instruction to spike - this is mandatory
    	* Keep the language syntax and semantics correct, else spike will spit errors. 

	type(str): Instruction type that needs to be added to the Spike
		* This `type` will be added to `riscv-isa-sim/spike_main/disasm.cc`
		* DEFINE_$TYPE can be any format as specified in the above directory
		* For Adding Instruction to spike - this is mandatory

	opcode(str): Opcode of the instruction that needs to be added to assembler
		* This `opcode` will be added to `riscv-gnu-toolchain/riscv-binutils-gdb/opcodes/riscv-opc.c`
		* Find references from the above directory for different opcode types
		* For Adding Instruction to assembler - this is mandatory

Example:
    .. highlight:: python
    .. code-block:: pythons::

    'mac':{
		'name':	'mac',
		'format': 'mac      rd rs1 rs2 31..25=1 14..12=0 6..2=0x1A 1..0=3',
		'behaviour':[
			"require_extension('M');",
			"reg_t tmp = sext_xlen(RS1 * RS2);",
			"WRITE_RD(sext_xlen(READ_REG(insn.rd()) + tmp));"
			],
		'type':'DEFINE_RTYPE(mac)',
		'opcode':'{"mac",       "I",   "d,s,t",  MATCH_MAC, MASK_MAC, match_opcode, 0 }'
	},

Note:	Follow python `Dictionary` Syntax to avoid errors creeping in.
See Also: Documentation of Adding Instruction to toolchain
"""
NewInstruction={
	#'mod':{
	#	'name': 'mod',
	#	'format': 'mod     rd rs1 rs2 31..25=1  14..12=0 6..2=0x1A 1..0=3',
	#	'behaviour':[
	#		"WRITE_RD(sext_xlen(RS1 % RS2));"
	#		],
	#	'type':'DEFINE_RTYPE(mod)',
        #        'opcode':'{"mod",        0, {"I", 0},   "d,s,t", MATCH_MOD, MASK_MOD, match_opcode, 0 }'
	#}
	# ,
	# 'mac':{
	# 	'name':	'mac',
	# 	'format': 'mac      rd rs1 rs2 31..25=1 14..12=0 6..2=0x1C 1..0=3',
	# 	'behaviour':[
	# 		"require_extension('M');",
	# 		"reg_t tmp = sext_xlen(RS1 * RS2);",
	# 		"WRITE_RD(sext_xlen(READ_REG(insn.rd()) + tmp));"
	# 		],
	# 	'type':'DEFINE_RTYPE(mac)',
	# 	'opcode':'{"mac",       "I",   "d,s,t",  MATCH_MAC, MASK_MAC, match_opcode, 0 }'
	# }
	'andn':{
		'name':	'andn',
		'format': 'andn      rd rs1 rs2 31..25=0x20 14..12=7 6..2=0x1C 1..0=3',
		'behaviour':[
			"WRITE_RD(RS1 & ~RS2);"
			],
		'type':'DEFINE_RTYPE(andn)',
		'opcode':'{"andn",      0, {"I", 0},   "d,s,t",  MATCH_ANDN, MASK_ANDN, match_opcode, 0 },'
	},
	'orn':{
		'name':	'orn',
		'format': 'orn      rd rs1 rs2 31..25=0x20 14..12=6 6..2=0x1C 1..0=3',
		'behaviour':[
			"WRITE_RD(RS1 | ~RS2);"
			],
		'type':'DEFINE_RTYPE(orn)',
		'opcode':'{"orn",       0, {"I", 0},   "d,s,t",  MATCH_ORN, MASK_ORN, match_opcode, 0 },'
	},
	'xnor':{
		'name':	'xnor',
		'format': 'xnor      rd rs1 rs2 31..25=0x20 14..12=4 6..2=0x1C 1..0=3',
		'behaviour':[
			"WRITE_RD(RS1 ^ ~RS2);"
			],
		'type':'DEFINE_RTYPE(xnor)',
		'opcode':'{"xnor",      0, {"I", 0},   "d,s,t",  MATCH_XNOR, MASK_XNOR, match_opcode, 0 },'
	},
	'rol':{
		'name':	'rol',
		'format': 'rol      rd rs1 rs2 31..25=48 14..12=1 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);",
			"int rshamt = -shamt & (xlen-1);",
			"WRITE_RD(sext_xlen((RS1 << shamt) | (zext_xlen(RS1) >> rshamt)));"
			],
		'type':'DEFINE_RTYPE(rol)',
		'opcode':'{"rol",       0, {"I", 0},   "d,s,t",  MATCH_ROL, MASK_ROL, match_opcode, 0 },'
	},
	'ror':{
		'name':	'ror',
		'format': 'ror      rd rs1 rs2 31..25=48 14..12=5 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);",
			"int rshamt = -shamt & (xlen-1);",
			"WRITE_RD(sext_xlen((RS1 << rshamt) | (zext_xlen(RS1) >> shamt)));"
			],
		'type':'DEFINE_RTYPE(ror)',
		'opcode':'{"ror",       0, {"I", 0},   "d,s,t",  MATCH_ROR, MASK_ROR, match_opcode, 0 },'
	},
	'slo':{
		'name':	'slo',
		'format': 'slo      rd rs1 rs2 31..25=16 14..12=1 6..2=0x0C 1..0=3',
		'behaviour':[
			"WRITE_RD(sext_xlen(~((~RS1) << (RS2 & (xlen-1)))));"
			],
		'type':'DEFINE_RTYPE(slo)',
		'opcode':'{"slo",       0, {"I", 0},   "d,s,t",  MATCH_SLO, MASK_SLO, match_opcode, 0 },'
	},
	'sro':{
		'name':	'sro',
		'format': 'sro      rd rs1 rs2 31..25=16 14..12=5 6..2=0x0C 1..0=3',
		'behaviour':[
			"WRITE_RD(sext_xlen(~((~zext_xlen(RS1)) >> (RS2 & (xlen-1)))));"
			],
		'type':'DEFINE_RTYPE(sro)',
		'opcode':'{"sro",       0, {"I", 0},   "d,s,t",  MATCH_SRO, MASK_SRO, match_opcode, 0 },'
	},
	'sbset':{
		'name':	'sbset',
		'format': 'sbset      rd rs1 rs2 31..25=20 14..12=1 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);"
			"WRITE_RD(sext_xlen(RS1 | (1LL << shamt)));"
			],
		'type':'DEFINE_RTYPE(sbset)',
		'opcode':'{"sbset",     0, {"I", 0},   "d,s,t",  MATCH_SBSET, MASK_SBSET, match_opcode, 0 },'
	},
	'sbclr':{
		'name':	'sbclr',
		'format': 'sbclr      rd rs1 rs2 31..25=36 14..12=1 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);"
			"WRITE_RD(sext_xlen(RS1 & ~(1LL << shamt)));"
			],
		'type':'DEFINE_RTYPE(sbclr)',
		'opcode':'{"sbclr",     0, {"I", 0},   "d,s,t",  MATCH_SBCLR, MASK_SBCLR, match_opcode, 0 },'
	},
	'sbinv':{
		'name':	'sbinv',
		'format': 'sbinv      rd rs1 rs2 31..25=52 14..12=1 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);"
			"WRITE_RD(sext_xlen(RS1 ^ (1LL << shamt)));"
			],
		'type':'DEFINE_RTYPE(sbinv)',
		'opcode':'{"sbinv",     0, {"I", 0},   "d,s,t",  MATCH_SBINV, MASK_SBINV, match_opcode, 0 },'
	},
	'sbext':{
		'name':	'sbext',
		'format': 'sbext      rd rs1 rs2 31..25=36 14..12=5 6..2=0x0C 1..0=3',
		'behaviour':[
			"int shamt = RS2 & (xlen-1);"
			"WRITE_RD(sext_xlen(1 & (RS1 >> shamt)));"
			],
		'type':'DEFINE_RTYPE(sbext)',
		'opcode':'{"sbext",     0, {"I", 0},   "d,s,t",  MATCH_SBEXT, MASK_SBEXT, match_opcode, 0 },'
	},
	'pack':{
		'name':	'pack',
		'format': 'pack      rd rs1 rs2 31..25=4 14..12=4 6..2=0x0C 1..0=3',
		'behaviour':[
			"reg_t lo = zext_xlen(RS1 << (xlen/2)) >> (xlen/2);"
			"reg_t hi = zext_xlen(RS2 << (xlen/2));"
			"WRITE_RD(sext_xlen(lo | hi));"
			],
		'type':'DEFINE_RTYPE(pack)',
		'opcode':'{"pack",      0, {"I", 0},   "d,s,t",  MATCH_PACK, MASK_PACK, match_opcode, 0 },'
	}
	# ,
	# 'cmix':{
	# 	'name':	'cmix',
	# 	'format': 'cmix       rd rs1 rs2 rs3 26..25=3 14..12=5 6..2=0x0C 1..0=3',
	# 	'behaviour':[
	# 		"WRITE_RD(RS2 ? RS1 : RS3);"
	# 		],
	# 	'type':'DEFINE_RTYPE(pack)', #yet to fix this
	# 	'opcode':'{"cmix",      0, {"I", 0},   "d,t,s,r",  MATCH_CMIX, MASK_CMIX, match_opcode, 0 },'
	# },
}
