#!/usr/bin/env python3
from common.log import logger
import re
import sys
import argparse
import subprocess
import fileinput
import os
from enum import Enum
from template.custom_instr_template import NewInstruction
from git import Repo


def func_name():
    """Returns a string with the name of the function it's called from"""
    return sys._getframe(1).f_code.co_name


riscv_tools_path = os.environ["RISCV_HOME"] + "/riscv-tools"
riscv_gnu_path = os.environ["RISCV_HOME"] + "/riscv-gnu-toolchain"
toolchain_path = "."
opcode_file_path = riscv_tools_path + "/riscv-opcodes"
compiler_path = riscv_gnu_path
spike_path = riscv_tools_path + "/riscv-isa-sim"
riscv_mk_in_file_path = spike_path + "/riscv"
instr_bhvr_path = riscv_mk_in_file_path + "/insns"
disasm_file_path = spike_path + "/spike_main"
riscv_opc_c_path1 = compiler_path + "/riscv-gdb/opcodes"
riscv_opc_c_path2 = compiler_path + "/riscv-binutils/opcodes"
riscv_opc_h_path1 = compiler_path + "/riscv-gdb/include/opcode"
riscv_opc_h_path2 = compiler_path + "/riscv-binutils/include/opcode"
encoding_h_path = riscv_mk_in_file_path

riscv_tools_repo = Repo(riscv_tools_path)
riscv_gnu_repo = Repo(riscv_gnu_path)
for submodule in riscv_tools_repo.iter_submodules():
    temp_path = "{0}/{1}".format(riscv_tools_path, submodule.path)
    if os.path.isdir(temp_path):
        os.chdir(temp_path)
        os.system("git checkout .")
        temp_repo = Repo(temp_path, search_parent_directories=True)
        changed = [item.a_path for item in temp_repo.index.diff(None)]
        if changed:
            for modified_file in changed:
                rm_file = "{0}/{1}".format(temp_path, modified_file)
                if os.path.isdir(rm_file):
                    sub_repo = Repo(rm_file, search_parent_directories=True)
                    os.chdir(rm_file)
                    sub_changed = [item.a_path for item in sub_repo.index.diff(None)]
                    if sub_changed:
                        for sub_file in sub_changed:
                            sub_rm_file = "{0}/{1}".format(rm_file, sub_file)
                            logger.debug(sub_rm_file)
                            os.remove(sub_rm_file)
                            os.system("git checkout .")
                else:
                    logger.debug(rm_file)
                    os.remove(rm_file)
                    os.system("git checkout .")

for submodule in riscv_gnu_repo.iter_submodules():
    temp_path = "{0}/{1}".format(riscv_gnu_path, submodule.path)
    if os.path.isdir(temp_path):
        os.chdir(temp_path)
        os.system("git checkout .")
        temp_repo = Repo(temp_path, search_parent_directories=True)
        changed = [item.a_path for item in temp_repo.index.diff(None)]
        if changed:
            for modified_file in changed:
                rm_file = "{0}/{1}".format(temp_path, modified_file)
                if os.path.isdir(rm_file):
                    sub_repo = Repo(rm_file, search_parent_directories=True)
                    os.chdir(rm_file)
                    sub_changed = [item.a_path for item in sub_repo.index.diff(None)]
                    if sub_changed:
                        for sub_file in sub_changed:
                            sub_rm_file = "{0}/{1}".format(rm_file, sub_file)
                            logger.debug(sub_rm_file)
                            os.remove(sub_rm_file)
                            os.system("git checkout .")
                else:
                    logger.debug(rm_file)
                    os.remove(rm_file)
                    os.system("git checkout .")

"""toolchain_path (str): Holds the path to toolchain

`toolchain_path` variable holds the directory accessibility. If errors are popping
up on running this script check this variable. Other variables depend on this one.
"""


class ErrorCodes(Enum):
    """Errorcodes while adding new Instruction

    To figure out cause of error while adding instruction to toolchain.
    Used while generating report at the end - i.e., after toolchain build

    Attributes:
        0 (int): If instruction is added succesfully .
        1 - 8 (`int`): If instruction fails to add and where exactly it failed.

    """

    INSTR_ADDED = 0  #: Instruction added successfully
    INSTR_EXISTS = 1  #: Instruction already found in toolchain
    INSTR_BHVR_FAIL = 2  #: failure in get_instruction_behaviour() function
    OPCOD_EDIT_FAIL = 3  #: failure in add_to_opcode_file() function
    MAKE_INSTL_FAIL = 4  #: failure in run_make_install() function
    MKIN_EDIT_FAIL = 5  #: failure in add_to_riscv_mkin_file() function
    DISASM_EDIT_FAIL = 6  #: failure in add_to_disasm_cc_file() function
    OPC_C_EDIT_FAIL = 7  #: failure in add_to_riscv_c_file() function
    OPC_H_EDIT_FAIL = 8  #: failure in add_to_riscv_h_file() function


def main(arguments):
    """Expects the template with New Instruction and parses it, to be added to 
    toolchain and finally builds the toolchain one-shot after which a report is
    generated

    Args:
        arguments (str): Not needed, yet kept here.
    """
    logger.debug("isa-extn: Adding Custom Instruction")
    result_list = {}  #: stores the result of each instruction added
    for key, value in NewInstruction.items():
        res = add_new_instruction(value)  #: res is an enum class output
        result_list.update({key: ["Not Added" if res.value else "Added", res.value]})
    logger.debug("Started Building Toolchain .. .. ..")
    build_tool_chain()
    logger.debug("Toolchain Built successfully\n")
    logger.info(
        "{:<15} | {:<15} | {:<8} | {:<30}".format(
            "Instruction", "status", "E-code", "E-state"
        )
    )
    for key, value in result_list.items():
        state, ecode = value
        logger.info(
            "{:<15} | {:<15} | {:<8} | {:<30}".format(
                key, state, ecode, ErrorCodes(ecode)
            )
        )


def exitpoint():
    """Unified Point of Exit.
    """
    logger.error("Operation Aborted. Exiting .. .. ..")
    sys.exit(1)


def add_new_instruction(Instr):
    """Core function in the module which calls all sub-functions that adds to 
    files in the toolchain before toolchain build is called.

    Notes:
        Functions are called in specific order
        1. unlist the data from the `Instr` to specifics
        2. add to `/opcodes` file
        3. run `make install` to generate `MATCH` & `MASK`
        4. Add necessities to spike
        5. Add necessities to Assembler

    Args:
        Instr (dict): Details of the Instruction.

    Returns:
        enum: list of class:: Errorcodes 

    .. _Github:
        https://github.com/riscv-mit/riscv-isa-sim/wiki/Adding-and-using-a-new-instruction
    .. _Google_group:
        https://groups.google.com/a/groups.riscv.org/forum/#!topic/sw-dev/nY3MFt83Wjw
    .. _Github_blog:
        https://nitish2112.github.io/post/adding-instruction-riscv/

    """
    logger.debug("Start Setup")
    instr_name = str(Instr.get("name")).lstrip()  # performed to stick to guidelines
    # if instruction_exists(instr_name):
    #    return ErrorCodes.INSTR_EXISTS
    logger.debug("Adding instruction: " + str(Instr.get("name")))
    instr_format = get_instruction_format(instr_name, Instr.get("format"))
    instr_type = get_instruction_type(Instr.get("type"))
    instr_opcode = get_instruction_opcode(Instr.get("opcode"))
    if not (get_instruction_behaviour(instr_name, Instr.get("behaviour"))):
        return ErrorCodes.INSTR_BHVR_FAIL
    if not (add_to_opcode_file(instr_name, instr_format)):
        return ErrorCodes.OPCOD_EDIT_FAIL
    if not (run_make_install()):
        return ErrorCodes.MAKE_INSTL_FAIL
    if not (add_to_riscv_mkin_file(instr_name)):
        return ErrorCodes.MKIN_EDIT_FAIL
    if not (add_to_disasm_cc_file(instr_type)):
        return ErrorCodes.DISASM_EDIT_FAIL
    if not (add_to_riscv_c_file(instr_opcode, riscv_opc_c_path1)):
        return ErrorCodes.OPC_C_EDIT_FAIL
    if not (add_to_riscv_c_file(instr_opcode, riscv_opc_c_path2)):
        return ErrorCodes.OPC_C_EDIT_FAIL
    if not (add_to_riscv_h_file(instr_name, riscv_opc_h_path1)):
        return ErrorCodes.OPC_H_EDIT_FAIL
    if not (add_to_riscv_h_file(instr_name, riscv_opc_h_path2)):
        return ErrorCodes.OPC_H_EDIT_FAIL
    logger.debug("DONE: Adding -{0}-".format(Instr.get("name")))
    return ErrorCodes.INSTR_ADDED


def get_instruction_format(instr_name, instr_form):
    """Function that aligns Instruction Format according to guidelines.

    Instruction given by the user, if have, instruction name as redundant, will 
    be stripped of along with the spaces. 

    Args:
        instr_name (str): Name of the Instruction.
        instr_form (str): Format given by user for the New Instruction.

    Returns:
        str: stripped version of Instruction Format

    """
    instr_format = str(instr_form).lstrip()
    if instr_format.startswith(instr_name):
        instr_format = instr_format[len(instr_name) :].lstrip()
    return instr_format


def get_instruction_behaviour(instr_name, instr_bhvr, file_path=instr_bhvr_path):
    """Function that gets behaviour from the user and writes into file
   
    This Instruction uses 'w' mode explicitly so if the file exists, it will be
    overridden, than actually appending.

    Args:
        instr_name  (str): Name of the Instruction.
        instr_bhvr (list): Format given by user for the New Instruction.
        file_path   (str): destination at which behaviour file needs to be stored

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Raises:
        Error: if file is not accessible

    Note:
        This function is helper for Spike - Instruction Addition.
    """
    is_success = False
    logger.debug(
        "[{0}] instr_name: {1}, instr_bhvr: {2}".format(
            func_name(), instr_name, instr_bhvr
        )
    )
    if os.path.isdir(file_path):
        file_name = instr_name + str(".h")
        try:
            subprocess.check_call("touch " + file_name, shell=True, cwd=file_path)
            f = open(file_path + "/" + file_name, "w")
            for eachline in instr_bhvr:
                print(eachline, file=f)
            f.close()
        except Exception as e:
            raise e
        else:
            is_success = True
    else:
        logger.error(str(file_path) + " not found Check whether directory is accessible")
        exitpoint()
    logger.info("[{0}] Created: {1}/{2}".format(func_name(), file_path, file_name))
    return is_success


def get_instruction_type(inst_type):
    """Function that aligns Instruction Type according to guidelines.

    Instruction Type given by the user, if instruction doesn't have `;`, will be
    added so that build_toolchain won't raise errors. A `double space` is added 
    as prefix to align in the file.

    Args:
        inst_type (str): Type of instruction given by user.

    Returns:
        str: stripped version of Instruction Type

    """
    format_type = str(inst_type).lstrip()
    if not (format_type.rstrip().endswith(";")):
        format_type = format_type + ";"
    format_type = "  " + format_type
    return format_type


def get_instruction_opcode(inst_opcod):
    """Function that aligns Instruction Opcode Type according to guidelines.

    New Instruction Opcode given by the user, if it isn't terminated by `,`, will
    be added so that build_toolchain won't raise errors. 

    Args:
        inst_opcod (str): Opcode of instruction given by user.

    Returns:
        str: stripped version of Instruction Opcode

    """
    OpCode = str(inst_opcod).lstrip()
    if not (OpCode.rstrip().endswith(",")):
        OpCode = OpCode + ","
    return OpCode


def add_to_opcode_file(instr_name, inst_format):
    """Function that adds Instruction name and it's format to `opcodes` file.

    Args:
        instr_name (str): Name of the Instruction
        inst_format(str): Bit Position Values of the Instruction
        file_name  (str): file name to which Instruction should be added

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Raises:
        Error: if file is not accessible

    Note:
        This function is helper for Spike & assembler - Instruction Addition.    

    """
    is_success = False
    file_name = opcode_file_path + "/opcodes"
    if os.path.isfile(file_name):
        try:
            f = open(file_name, "a+")
            helpword = "\t# Generated by isa-extn "
            #: `helpword` is used to find the additions in file by the script
            print(instr_name + "\t" + inst_format + helpword, file=f)
            f.close()
        except Exception as e:
            raise e
        else:
            is_success = True
    else:
        logger.error("ERROR: File not found,".format(file_name))
        exitpoint()
    logger.info("Edited {}".format(file_name))
    return is_success


def run_make_install(dir_path=opcode_file_path):
    """Function that runs `make install` in `riscv-opcodes` directory.
    
    `make install` will update all the headers with new instruction mask and mod
    
    Args:
        dir_path (str): directory at which `make install` should run

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Raises:
        Error: if `make` fails

    Note:
        This function is helper for Spike & assembler - Instruction Addition.    

    """
    is_success = False
    if os.path.isdir(dir_path):
        logger.info(
            "[{0}] Updating encoding.h at {1}".format(func_name(), opcode_file_path)
        )
        os.system("cp ~/Makefile {0}".format(opcode_file_path))
        try:
            subprocess.check_call(["make", "install"], shell=True, cwd=dir_path)
        except Exception as e:
            raise e
        else:
            is_success = True
    else:
        logger.error(
            str(dir_path) + " not found Check whether directory Exists (is accessible ?) "
        )
        exitpoint()
    return is_success


def add_to_riscv_mkin_file(instr_name, file_path=riscv_mk_in_file_path):
    """Function that writes to riscv.mk.in file
   
    adds the instruction name to the struct `riscv_insn_list`. Keeps prepending 
    at top of the struct

    Args:
        instr_name  (str): Name of the Instruction.
        file_path   (str): riscv.mk.in file destination

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Note:
        This function is helper for Spike - Instruction Addition.
    """
    is_success = False
    if os.path.isfile(file_path + str("/riscv.mk.in")):
        logger.debug("[{0}] Editing file {1}/riscv.mk.in".format(func_name(), file_path))
        for line in fileinput.input(str(file_path + str("/riscv.mk.in")), inplace=True):
            print(
                line.replace(
                    "riscv_insn_list = \\",
                    "riscv_insn_list = \\\n\t" + str(instr_name) + " \\",
                ),
                end="",
            )
        is_success = True
    else:
        logger.error(
            "[{0}] File not found {0}/riscv.mk.in".format(func_name(), file_path)
        )
        exitpoint()
    return is_success


def add_to_disasm_cc_file(instr_type, file_path=disasm_file_path):
    """Function that writes to disasm.cc file
   
    adds the DEFINE_$TYPE to disasm.cc. appends after `DEFINE_FXTYPE(fle_q);` in file
    Each time `type` is added, a comment ' //Added by Toolchain Script' follows it

    Args:
        instr_type (str): Type of the Instruction.
        file_path   (str): disasm.cc file destination

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Note:
        This function is helper for Spike - Instruction Addition.
    """
    is_success = False
    file_name = file_path + str("/disasm.cc")
    if os.path.isfile(file_name):
        logger.debug(
            "[{0}] {1} will be added to {2}".format(func_name(), instr_type, file_name)
        )
        for line in fileinput.input(file_name, inplace=True):
#           if line.startswith("  DEFINE_FX2TYPE(fle_q);"): # for latest riscv-isa-sim
            if line.startswith("  DEFINE_FXTYPE(fle_q);"):
                line = line.replace(
                    line, line + "\n" + instr_type + "\t // Generated by isa-extn "
                )
            print(line, end="")
        is_success = True
    else:
        logger.error("[{0}] File not found: {1}".format(func_name(), file_path))
        exitpoint()
    return is_success


def add_to_riscv_c_file(OpCode, file_path=riscv_opc_c_path1):
    """Function that writes to riscv-opc.c file
   
    adds the opcode to `riscv-opc.c` at the end of the file.
    Each time `opcode` is added, a comment ' //Added by Toolchain Script' follows it

    Args:
        OpCode    (str): Opcode of the Instruction.
        file_path (str): riscv-opc.c file destination

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Note:
        This function is helper for Assembler - Instruction Addition.
    """
    is_success = False
    file_name = file_path + str("/riscv-opc.c")
    if os.path.isfile(file_name):
        logger.debug("[{0}] {1} added to {2}".format(func_name(), OpCode, file_name))
        for line in fileinput.input(file_name, inplace=True):
            if line.startswith('{"wfi",'):
                line = line.replace(
                    line, line + "\n" + OpCode + "\t // Generated by isa-extn " + "\n"
                )
            print(line, end="")
        is_success = True
    else:
        logger.error("[{0}] File not found: {1}".format(func_name(), file_path))
        exitpoint()
    return is_success


def add_to_riscv_h_file(instr_name, destfile_path=riscv_opc_h_path1):
    """Function that writes to riscv-opc.h file
   
    Performs two operations -
        1. Extracts the MATCH and MOD by calling `extract_mask_mod()` function 
        2. returned list is added to `riscv-opc.h` at appropriate places.
    For each addition, a comment ' //Added by Toolchain Script' follows it

    Args:
        instr_name     (str): Opcode of the Instruction.
        destfile_path  (str): riscv-opc.h file destination

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Note:
        This function is helper for Assembler - Instruction Addition.
    """
    is_success = False
    destfile_name = destfile_path + str("/riscv-opc.h")
    logger.debug("[{0}] checking {1}".format(func_name(), destfile_name))
    if os.path.isfile(destfile_name):
        line_list = []  #: holds Match, mod and insns lines from encoding.h
        line_list = extract_mask_mod(instr_name)
        if line_list:
            logger.debug(
                "[{0}] Pasting -{1}- match and mask in {2}".format(
                    func_name(), instr_name, destfile_name
                )
            )
            helpword = "\t /* Generated by isa-extn  */\n"
            for line in fileinput.input(destfile_name, inplace=True):
                if line.startswith("#define RISCV_ENCODING_H"):
                    line = line.replace(
                        line, line + "\n" + line_list[0] + helpword + line_list[1]
                    )
                elif line.startswith("#ifdef DECLARE_INSN"):
                    line = line.replace(line, line + "\n" + line_list[2] + helpword)
                print(line, end="")
            is_success = True
        else:
            logger.error("Match and Mask files not found ")
            exitpoint()
    else:
        logger.error("[{0}] File not found: {1}".format(func_name(), destfile_path))
        exitpoint()
    return is_success


def extract_mask_mod(instr_name, srcfile_path=encoding_h_path):
    """Function that extracts from encoding.h file
   
    Performs two operations: 
        1) Extracts the MATCH and MOD by calling `extract_mask_mod()` function 
        2) returned list is added to `riscv-opc.h` at appropriate places.
    For each addition, a comment ' //Added by Toolchain Script' follows it

    Args:
        instr_name   (str): Opcode of the Instruction.
        srcfile_path (str): encoding.h file destination

    Returns:
        list: containing match,mod and insns of the new instruction, that will be
        added to the `riscv-opc.h` file

    Note:
        This function is helper for Assembler - Instruction Addition.
        This function depends on spike's `riscv-isa-sim` directory
    """
    srcfile_name = srcfile_path + str("/encoding.h")
    list_of_lines = []  #: copies match, mod and insns into this list.
    if os.path.isfile(srcfile_name):
        logger.debug(
            "[{0}] Extracting -{1}- match and mask from {2}".format(
                func_name(), instr_name, srcfile_name
            )
        )
        for line in fileinput.input(srcfile_name, inplace=True):
            if line.startswith("#define MATCH_" + str(instr_name).upper()):
                list_of_lines.append(line)
            elif line.startswith("#define MASK_" + str(instr_name).upper()):
                list_of_lines.append(line)
            elif line.startswith("DECLARE_INSN(" + str(instr_name).lower()):
                list_of_lines.append(line)
            print(line, end="")
    else:
        logger.error("[{0}] File not found: {1}".format(func_name(), srcfile_path))
        exitpoint()
    return list_of_lines


def build_tool_chain(file_path=toolchain_path):
    """Function that initiates building the toolchain
   
    make for building the isa-sim and toolchain
    
    Args:
        file_path (str): make file destination

    Returns:
        bool: The return value. True if succeeds, False otherwise.

    Raises:
        Error: if `build` fails

    """
    is_success = False
    spike_build_path = spike_path + "/build"
    if os.path.isdir(compiler_path) & os.path.isdir(spike_build_path):
        logger.debug("Rebuilding Toolchain with New Instruction \n")
        try:
            subprocess.check_call(["make", "clean"], shell=True, cwd=compiler_path)
            subprocess.check_call(["make"], shell=True, cwd=compiler_path)
            subprocess.check_call(["make", "clean"], shell=True, cwd=spike_build_path)
            subprocess.check_call(["make"], shell=True, cwd=spike_build_path)
            subprocess.check_call(["make", "install"], shell=True, cwd=spike_build_path)
        #            subprocess.check_call("./build.sh", shell=True, cwd=file_path)
        except Exception as e:
            raise e
        else:
            is_success = True
    else:
        logger.error(
            str(compiler_path)
            + "\nor\n"
            + str(spike_build_path)
            + "\n not found Check whether directory Exists (is accessible ?) "
        )
        exitpoint()
    return is_success


def instruction_exists(instr_name):
    """Checks whether instruction exists in the toolchain.

    Checks the `opcodes`, `riscv.mk.in`, `riscv-opc.c`, `riscv-opc.h` & `disasm.cc`
    files for the instruction's existence.
    files and the instruction name traces are arranged in Dict format, which is 
    iterated making function calls to sub-helper function `search_in_file()`

    Args:
        instr_name (str): Name of the Instruction to check in toolchain.

    Returns:
        bool: The return value. True if Found, False otherwise.

    """
    file_list = {
        opcode_file_path + str("/opcodes"): instr_name,
        riscv_mk_in_file_path + str("/riscv.mk.in"): instr_name,
        riscv_opc_c_path1 + str("/riscv-opc.c"): '{"' + instr_name + '",',
        riscv_opc_c_path2 + str("/riscv-opc.c"): '{"' + instr_name + '",',
        riscv_opc_h_path1
        + str("/riscv-opc.h"): "#define MATCH_"
        + str(instr_name).upper(),
        riscv_opc_h_path2
        + str("/riscv-opc.h"): "#define MATCH_"
        + str(instr_name).upper(),
    }  #: list of files and search term as dict
    found_instr = False
    for key, value in file_list.items():
        if search_in_file(key, value, True):
            found_instr = True
            logger.debug("\tfound '" + str(instr_name) + "' in " + str(key))
            break
    else:
        key = disasm_file_path + str("/disasm.cc")
        if search_in_file(key, instr_name + ");", False):
            found_instr = True
            logger.debug("\tfound '" + str(instr_name) + "' in " + str(key))
    return found_instr


def search_in_file(file_name, search_term, at_start):
    """Private Function that searches the given `search_term` in `file_name`.

    Uses the infile to avoid memory clog and efficient usage of garbage collection
    mechanism provided by python.
    
    Args:
        file_name (str): File in which instruction traces needs to be found.
        search_term(str): Instruction Traces in a file
        at_start (bool): Specifies whether search_term is at the start of line in file

    Returns:
        bool: The return value. True if Found, False otherwise.

    """
    found_flag = False
    try:
        with open(file_name) as infile:
            for line in infile:
                if at_start:
                    if line.lstrip().startswith(search_term):
                        found_flag = True
                        break
                else:
                    if line.find(search_term) != -1:
                        found_flag = True
                        break
    except Exception as e:
        raise e
    return found_flag


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
