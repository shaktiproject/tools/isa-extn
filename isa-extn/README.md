# Adding Custom Instruction to Toolchain

Guide from [GitHub](https://github.com/riscv-mit/riscv-isa-sim/wiki/Adding-and-using-a-new-instruction), [blog](https://nitish2112.github.io/post/adding-instruction-riscv/) & [group](https://groups.google.com/a/groups.riscv.org/forum/#!topic/sw-dev/nY3MFt83Wjw) were followed.
## Prerequisite
The user is expected to have a directory called `riscv-tools` or similar one which should have contents in it like
```
riscv-tools
  │
  └───riscv-opcodes
  │    │    opcodes
  │    │    ...
  │
  └───riscv-gnu-toolchain
  │    |
  │    └─── riscv-binutils-gdb
  │    │    │
  │    │    └─── include
  │    │    │    │
  │    │    │    └─── opcode
  │    │    │    │    │    riscv-opc.h
  │    │    │    │    │    ...
  │    │    │    │    ...
  │    │    │
  │    │    └─── opcodes
  │    │    │    │   riscv-opc.c
  │    │    │    │   ...
  │    │    │    ...
  │    │    ...
  │
  └───riscv-isa-sim
  │    │
  │    └───riscv
  │    │    │
  │    │    └───insns
  │    │    │    │    <NEW-INSTRUCTION>.h
  │    │    │    │    ...
  │    │    │    │
  │    │    |    riscv.mk.in
  │    │    |
  │    └───spike_main
  │    │    │   disasm.cc
  │    │    │   ...
  │
  └─── template                       <----- Paste directory here
  │    │   custom_instr_template.py
  │    │   ...
  |
  |    add_custom_instr.py            <----- Script in this repo should be here
  │    build.sh
  |    ...
```
Expecting the script to be inside `riscv-tools` as shown above.<br>
__It is assumed that riscv-toolchain with standard instructions were built, before
running this script__ <br>
To build toolchain  - cd to `/riscv-tools`
```shell
echo $RISCV
./build.sh
```
*`$RISCV` is where the toolchain will be installed*
## Implementation
implemented in 5 phases
* Template Parsing to get instruction details
* Adding instruction to `opcode` and generate `match` & `mask` information
* Adding instruction to Spike
* Adding instruction to Assembler
* Rebuilding the toolchain

## Usage
*Before running the file, add your new instruction to the [custom_instr_template.py](template/custom_instr_template.py) file*
<br>Directions to add new instruction are described [here](template/README.md)

```shell
$ time python3 add_custom_instr.py
```
*__time__ is used to figure out the time utilized for building toolchain*

*Files specified in the above directory structure will be edited by the script, which can be found out by __Added by Toolchain Script__ comment*

## Example
Running a `mod` instruction whose semantics are:
```
mod r1, r2, r3

Semantics:
R[r1] = R[r2] % R[r3]
```
Executing the [file](main.c) which contains this instruction
```shell
$ riscv64-unknown-elf-gcc main.c -o main.elf
```
will result in
```
main.c: Assembler messages:
main.c:7: Error: unrecognized opcode `mod a5,a5,a4'
```
Editing the [custom_instr_template.py](template/custom_instr_template.py) with `mod` instruction and execute
```shell
$ time python3 add_custom_instr.py
```
will result in
```
~###~ Adding Custom Instructions to toolchain ~###~
~~ Adding 'mod' Instruction to toolchain ~~
.
.

RISC-V Toolchain installation completed!
Toolchain Built successfully

Instruction     | status          | E-code   | E-state                       
mod             | Added           | 0        | ErrorCodes.INSTR_ADDED        

real	73m29.437s
user	66m32.618s
sys	5m27.462s
```
Now rerun the [file](main.c)
```shell
$ riscv64-unknown-elf-gcc main.c -o main.elf
```
To see the `mod` instruction used by toolchain
```shell
$ riscv64-unknown-elf-objdump -dC main.elf > mod.disass
```
check the `mod.disass` file for the new instruction executed by assembler<br>
check for `main` section which will look like
```
00000000000101e4 <main>:
   101e4:	fe010113          	addi	sp,sp,-32
   101e8:	00113c23          	sd	ra,24(sp)
   .
   .

   10208:	fe842703          	lw	a4,-24(s0)   
   1020c:	02e787eb          	mod	a5,a5,a4   
   10210:	fef42223          	sw	a5,-28(s0)
   .
   .   
   10258:	00008067          	ret

```

Run RISC-V Simulator with the custom instruction
```shell
spike -l pk main.elf 2>mod.dump
```
find the new instruction in spike dump which will look like <br>
```
core   0: 0x0000000000010208 (0xfe842703) lw      a4, -24(s0)
core   0: 0x000000000001020c (0x02e787eb) mod     a5, a5, a4
core   0: 0x0000000000010210 (0xfef42223) sw      a5, -28(s0)
```
you can run the file as baremetal code and simulate it on spike
